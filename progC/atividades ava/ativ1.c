#include <stdio.h>

float calc_area(float l1, float l2){
    float area;
    area = l1 * l2;
    return area;
}

int main(){

    float lado, lado2, area1;

    printf("Escreva o valor dos lados:\n");
    scanf("%f %f", &lado, &lado2);
    area1 = calc_area(lado, lado2);
    printf("O valor da area:\n%f", area1);

    if (area1 > 10){
        printf("\nA area possui mais de 10m2");
    } else {
        printf("\nA area possui menos de 10m2");
    }

}