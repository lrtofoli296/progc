#include <stdio.h>

float calc_nota(float ae, float tps, float asae, float pe){
    float np;
    np = (ae*0.3) + (tps*0.4) + (asae*0.3) + pe;
    return np;
}

float calc_mf(float np, float pf){
    float mf;
    mf = (np*0.5) + (pf*0.5);
    return mf;
}

int main(){

    //nota minima 0 e máxima 100, média minima para aprovar = 60
    float ae, tps, asae, pe, pf, np, mf;

    printf("Escreva sua nota AE, TPS, ASAE e PE respectivamente:\n");
    scanf("%f %f %f %f", &ae, &tps, &asae, &pe);
    
    np = calc_nota(ae, tps, asae, pe);

    printf("Nota parcial do aluno = %f\n", np);

    if( np >= 60){
        printf("Aluno aprovado!");
    } else {
        printf("Escreva a nota da prova final:\n");
        scanf("%f", &pf);
        mf = calc_mf(np, pf);
        //mf = (np*0,5) + (pf*0,5);
        printf("Media final do aluno = %f\n", mf);
        if(mf >= 60){
            printf("Aluno aprovado na media final!");
        } else {
            printf("Aluno reprovado!");
        }
    }

}